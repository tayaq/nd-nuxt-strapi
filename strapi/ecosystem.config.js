module.exports = {
  apps: [
    {
      name: 'ND Strapi',
      script: 'npm',
      args: 'start',
    },
  ],
};

const qs = require('qs');

export const state = () => ({
    headers: [],
    setting: {}
})

export const mutations = {
    setHeaders(state, headers) {
        state.headers = headers;
    },
    setSetting(state, setting) {
        state.setting = setting;
    },
}

export const actions = {

    async fetchHeaders({ commit}) {
        const query = qs.stringify({
            populate: [
                'background', 'seo'
            ]
        }, {
            encodeValuesOnly: true,
        });
        const response = await this.$axios.get(`/headers/?${query}`);
        await commit('setHeaders', response.data.data);
    },

    async fetchSetting({ commit }) {
        const query = qs.stringify({
            populate: [
                'contacts', 'og', 'og.image', 'favicon', 'faviconDark',
                'documents', 'documents.file'
            ]
        }, {
            encodeValuesOnly: true,
        });
        const response = await this.$axios.get(`/setting/?${query}`);
        await commit('setSetting', response.data.data);
    },

    async nuxtServerInit({ dispatch }) {
        await dispatch('fetchHeaders');
        await dispatch('fetchSetting');
    }
}


export const getters = {
    headers: state => state.headers,
    setting: state => state.setting,
}

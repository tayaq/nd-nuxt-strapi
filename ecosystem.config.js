module.exports = {
    apps: [
        {
            name: 'ND Nuxt',
            script: './node_modules/nuxt/bin/nuxt.js',
            args: 'start'
        }
    ]
}

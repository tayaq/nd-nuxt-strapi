if (document.body.classList.contains('page--article')) {

    const animationItems = document.querySelectorAll('.title__small, .article__img, .article-default-text, .article-left-text, .article__date, .article__share');

    for (let item of animationItems) {
        gsap.fromTo(item, { autoAlpha: 0, y: '15%' }, {
            duration: .5,
            autoAlpha: 1,
            y: 0,
            scrollTrigger: {
                trigger: item,
                start: 'top 90%',
                once: true
            }
        });
    }
}

if (document.body.classList.contains('page--contacts')) {

    const animationItems = document.querySelectorAll('.contacts__company, .contacts__press, .contacts__linkedin, .contacts__legal, .form, .mapbox');

    for (let item of animationItems) {
        gsap.fromTo(item, { autoAlpha: 0, y: '15%' }, {
            duration: .5,
            autoAlpha: 1,
            y: 0,
            scrollTrigger: {
                trigger: item,
                start: 'top 90%',
                once: true
            }
        });
    }

    mapboxgl.accessToken = 'pk.eyJ1IjoibmRncm91cCIsImEiOiJja3l1YmlmMjkwNW04MndzMmF2eDNydmFwIn0.3-y4iFhO83S2NdRFKwJQHg';
    var map = new mapboxgl.Map({
        container: 'mapbox',
        style: 'mapbox://styles/ndgroup/ckyubp8n4001m14t8tcef6cfi',
        center: [5.40066358644490, 51.45548178172814],
        zoom: '6'
    });

    map.on('load', () => {
        map.loadImage(
            '../images/marker.png',
            (error, image) => {
                if (error) throw error;
                map.addImage('mapmarker', image);
                map.addSource('point', {
                    'type': 'geojson',
                    'data': {
                        'type': 'FeatureCollection',
                        'features': [
                            {
                                'type': 'Feature',
                                'geometry': {
                                    'type': 'Point',
                                    'coordinates': [5.40066358644490, 51.45548178172814]
                                }
                            }
                        ]
                    }
                });

// Add a layer to use the image to represent the data.
                map.addLayer({
                    'id': 'points',
                    'type': 'symbol',
                    'source': 'point', // reference the data source
                    'layout': {
                        'icon-image': 'mapmarker', // reference the image
                        'icon-size': 0.39
                    }
                });
            }
        );
    });

}

if (document.body.classList.contains('page--news')) {

    const animationItems = document.querySelectorAll('.news__item, .news__title');

    for (let item of animationItems) {
        gsap.fromTo(item, { autoAlpha: 0, y: '15%' }, {
            duration: .5,
            autoAlpha: 1,
            y: 0,
            scrollTrigger: {
                trigger: item,
                start: 'top 90%',
                once: true
            }
        });
    }
}

if (document.body.classList.contains('page--main')) {

    const casesItems = document.querySelectorAll('.cases__item');

    for (let item of casesItems) {
        item.addEventListener('mouseenter', () => {
            item.querySelector('.cases__video').play();
        });

        item.addEventListener('mouseleave', () => {
            item.querySelector('.cases__video').pause();
        });
    }

    let missionsList = document.querySelector('.missions__list');
    let missionsProgress = document.querySelector('.missions__value');
    let newsList = document.querySelector('.short-news__list');


    ScrollTrigger.matchMedia({
        '(max-width: 767px)': () => {
            gsap.to(missionsList, {
                x: () => -(missionsList.offsetWidth + document.documentElement.clientWidth) + "px",
                ease: "none",
                scrollTrigger: {
                    trigger: '.missions__wrapper',
                    pin: true,
                    start: 'bottom bottom',
                    pinSpacing: true,
                    scrub: 1,
                    onUpdate: (e) => {
                        let index = getIndex(e.progress);
                        addClass(index);
                        missionsProgress.style.width = `${e.progress * 100}%`;
                    },
                    onRefreshInit: () => {
                        addClass(0);
                    },
                    end: () => "+=" + missionsList.offsetWidth * 2,
                },
            })
        },
        '(min-width: 768px) and (max-width: 1199px)': () => {
            gsap.to(missionsList, {
                x: () => -(missionsList.offsetWidth + document.documentElement.clientWidth) + "px",
                ease: "none",
                scrollTrigger: {
                    trigger: missionsList,
                    pin: '.page__content',
                    start: 'bottom bottom',
                    pinSpacing: false,
                    scrub: 1,
                    onUpdate: (e) => {
                        let index = getIndex(e.progress);
                        addClass(index);
                        missionsProgress.style.width = `${e.progress * 100}%`;
                    },
                    onRefreshInit: () => {
                        addClass(0);
                    },
                    end: () => "+=" + missionsList.offsetWidth,
                },
            })
        },
        '(min-width: 1200px)': () => {
            gsap.to(missionsList, {
                ease: "sine.out",
                scrollTrigger: {
                    pin: true,
                    toggleActions: "restart pause resume pause",
                    trigger: '.missions',
                    start: 'bottom bottom',
                    scrub: 1,
                    pinSpacing: true,
                    onUpdate: (e) => {
                        let index = getIndex(e.progress);
                        addClass(index)
                        missionsProgress.style.width = `${e.progress * 100}%`;
                    },
                    onRefreshInit: () => {
                        addClass(0);
                    },
                    end: () => "+=" + missionsList.offsetWidth * 2,
                },
            })
        },
    });

    ScrollTrigger.matchMedia({
        '(max-width: 767px)': () => {
            gsap.to(newsList, {
                x: () => -(newsList.scrollWidth - document.documentElement.clientWidth) - 40 + "px",
                ease: "none",
                scrollTrigger: {
                    trigger: '.short-news',
                    pin: true,
                    anticipatePin: 1,
                    scrub: .1,
                    pinSpacing: true,
                    start: `bottom bottom`,
                    end: () => "+=" + newsList.offsetWidth,
                }
            });
        },
        '(min-width: 768px) and (max-width: 1199px)': () => {
            gsap.to(newsList, {
                x: () => -(newsList.scrollWidth - document.documentElement.clientWidth) - 64 + "px",
                ease: "none",
                scrollTrigger: {
                    trigger: newsList,

                    pin: '.page__content',
                    anticipatePin: 1,
                    scrub: .1,
                    start: `top -${missionsList.offsetWidth - 245}px`,
                    end: () => "+=" + newsList.offsetWidth,
                }
            });
        },
        '(min-width: 1200px)': () => {
            gsap.to(newsList, {
                x: () => -(missionsList.offsetWidth / 2 + document.documentElement.clientWidth) + "px",
                ease: "none",
                scrollTrigger: {
                    trigger: '.short-news',
                    pin: true,
                    scrub: 1,
                    pinSpacing: true,
                    start: 'bottom bottom',
                    end: () => "+=" + newsList.offsetWidth,
                }
            });
        },
    });

    function getIndex(progress) {
        let index;
        if (progress < 1 / 3) index = 0;
        else if (progress > 1 / 3 && progress < 1 - 1 / 3) index = 1;
        else index = 2;
        return index;
    }

    function addClass(index) {
        document.querySelectorAll('.missions__current')[index].innerHTML = index + 1;
        let activeMissions = document.querySelector('.missions__item--active');
        if (activeMissions) activeMissions.classList.remove('missions__item--active');
        document.querySelectorAll('.missions__item')[index].classList.add('missions__item--active');
    }

    const animationItems = document.querySelectorAll('.main-about__message, .main-about__desc, .main-about__link, .missions__title, .missions__progress, .missions__list, .cases__line, .partners__logo, .partners__more, .short-news__item, .short-news__more');

    for (let item of animationItems) {
        gsap.fromTo(item, { autoAlpha: 0, y: '15%' }, {
            delay: item.dataset.delay || 0,
            duration: .5,
            autoAlpha: 1,
            y: 0,
            stagger: '.5',
            scrollTrigger: {
                trigger: item,
                start: 'top 90%',
                once: true
            }
        });
    }

    for (let item of casesItems) {
        gsap.fromTo(item, { autoAlpha: 0, y: '15%' }, {
            delay: (window.innerWidth > 768) ? item.dataset.delay || 0 : 0,
            duration: .5,
            autoAlpha: 1,
            y: 0,
            stagger: '.5',
            scrollTrigger: {
                trigger: item,
                start: 'top 90%',
                once: true
            }
        });
    }

}
